import React from "react"
import axios from "axios"

const End_Point="https://google-jobs.herokuapp.com/"

export async function  postServiceCall(URL,dataObject){
    const prepareURL=End_Point+URL;
    console.log("api URl",prepareURL,"DataObject",dataObject)
    let response=await axios.post(prepareURL,dataObject).then(res=>{
        if(res.status===200){
            res.data.data.status="success"
            return res.data.data;
        }
        else{
            res.status="fail";
            return res;
        }
    })
    return response;
}

export async function  getServiceCall(URL,dataObject){
    const prepareURL=End_Point+URL;
    console.log("api URl",prepareURL)
    let response=await axios.get(prepareURL).then(res=>{
        if(res.status===200){
            res.data.data.status="success"
            return res.data.data;
        }
        else{
            res.status="fail";
            return res;
        }
    })
    return response;
}