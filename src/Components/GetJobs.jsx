import React, { Component } from 'react'
import "./GetJobs.css"
import { postServiceCall, getServiceCall } from './serviceCalls/ServiceCalls'
import SingleSelectDropdown from './Utils/SemanticUIComponents/SingleSelectDropDown'
import { getDistinctValues, globalHardCodedConstants, getPagedData } from './Utils/SemanticUIComponents/Helpers'
import Job from './Job'
import Loader from './Utils/Loader'
import Pagination from './Utils/Pagination'

export class GetJobs extends Component {
    constructor(props) {
        super(props)

        this.state = {
            handleFilterMenu: false,
            positions: [],
            Companies: [],
            Locations: [],
            jobs: [],
            searchObject: {
            },
            currentPage: 1

        }
    }

    async componentDidMount() {
        this.getJobs();
        let Attributes = await getServiceCall("GetAttributes");
        let positions = getDistinctValues(Attributes, "JobTitle")
        let Companies = getDistinctValues(Attributes, "CompanyName")
        let Locations = getDistinctValues(Attributes, "Location")
        this.setState({ positions, Companies, Locations })
        console.log("jobs", positions)
    }

    handlePageChange = currentPage => {
            this.setState({ currentPage });
    };

    async getJobs() {
        let jobs = await postServiceCall("getJobList", this.state.searchObject);
        this.setState({ jobs })
    }
    handleFilterMenu = () => {
        this.setState({ handleFilterMenu: !this.state.handleFilterMenu, searchObject: {} })
        // this.getJobs();
    }
    handleSearchObject = async (e, { name, value }) => {
        this.setState({ jobs: [] ,currentPage:1})
        let searchObject = this.state.searchObject;
        searchObject[name] = value;
        this.setState({ searchObject })
        this.getJobs();
    }
    render() {
        console.log("state", this.state)
        let jobsJSX;
        let jobs = this.state.jobs;
        if (this.state.jobs.length > globalHardCodedConstants("PAGINATION_ITEMS_LENGTH")) {
            jobs = getPagedData(
                jobs,
                this.state.currentPage
            );
        }

        if (this.state.jobs.status !== "success") {
            jobsJSX = <Loader />
        }
        else {

            if (jobs.length > 0) {
                jobsJSX = jobs.map((job, index) => {
                    return (
                        <Job item={job} />
                    )
                })
            }
            else {
                jobsJSX = (
                    <p>No records found.</p>
                )
            }
        }


        return (
            <div className="">
                <span class="btn btn-primary btn-sm blue_bg pull-right " data-toggle="modal" data-target="#marmastructure" onClick={this.handleFilterMenu}> <i class="fa fa-filter"></i> Filters</span>

                {/* <h1 className="heading">Welcome</h1> */}
                <div className="">
                    {/* <button class="btn btn-primary btn-sm blue_bg pull-right " ></button> */}
                </div>

                <div className="margin50">

                    <div className="col-md-12 mt50">
                        <div className="">
                            {/* <ul>
                           <li>
                               
                           </li>
                       </ul> */}
                            {jobsJSX}

                        </div>
                        {this.state.jobs.length > globalHardCodedConstants("PAGINATION_ITEMS_LENGTH") &&
                            <Pagination
                                totalItemsCount={this.state.jobs.length}
                                currentPage={this.state.currentPage}
                                onPageChange={this.handlePageChange}
                            />
                        }
                    </div>
                    {/* <div class="col-md-12"><div class="health-table-heading panel-default "><div class="col-md-12"><div class="appetite-heading "><h3>VATA<span></span></h3></div><div class="add-btn accordion_btn"><button class="button" data-toggle="modal" data-target="#pathologyadd"><i class="fa fa-plus"></i> Add</button></div></div><div class="table-responsive teble-show collapse in" id="vatapathology" aria-expanded="true"><table class="table table-bordered-imp  table_hd_bd_bordered"><tbody><tr class="table-heading-bg"><th>Stage</th><th>Symptom</th><th width="10%">Sub Dosha</th><th width="10%">Dhatu</th><th width="10%">Malas</th><th>Srota</th><th>Herb Category</th><th>Herbs</th><th width="7%">Actions</th></tr><tr class="no-color"><td>A/A </td><td>PPM Constipation</td><td>APANA VAYU</td><td>RASA DHATU </td><td></td><td>PUREESHA VAHA</td><td>Laxative Carminative</td><td>Triphala, Psyllium, Flaxseed, Ginger, Asafoetida, Black Pepper</td><td></td></tr><tr><td>OVERFLOW</td><td>MT Systemic Dryness </td><td>VYANA VAYU</td><td> RASA DHATU </td><td></td><td>RASA VAHA</td><td>Demulcents</td><td>Licorice, Slippery Elm, Shatavari</td><td></td></tr><tr class="no-color"><td>OVERFLOW</td><td>MT Feel Cold and Fatigued</td><td>VYANA VAYU</td><td>RAKTA DHATU</td><td></td><td> RAKTA VAHA</td><td>Circulatory Stimunlants</td><td>Ginger, Cinnamon, Black Pepper</td><td></td></tr></tbody></table></div><div class="clearfix"></div></div></div> */}
                </div>


                {this.state.handleFilterMenu ?
                    <div class={
                        this.state.handleFilterMenu === true ?
                            "marmafilterdiv active"
                            :
                            "marmafilterdiv "
                    }>
                        <div class="marmafilterheader">
                            <h4>Filters <i class="fa fa-times pull-right" onClick={this.handleFilterMenu}></i></h4>
                        </div>
                        <div class="marmafilterbody">
                            <div class="form-group text-left">
                                <label class="control-label">Position</label>
                                <SingleSelectDropdown
                                    placeholder="Select Position"
                                    options={this.state.positions}
                                    name="JobTitle"
                                    onChange={this.handleSearchObject}
                                    value={this.state.searchObject.JobTitle}
                                />

                            </div>
                            <div class="form-group text-left">
                                <label class="control-label">Company</label>

                                <SingleSelectDropdown
                                    placeholder="Select Comapny"
                                    options={this.state.Companies}
                                    name="CompanyName"
                                    onChange={this.handleSearchObject}
                                    value={this.state.searchObject.CompanyName}
                                />
                            </div>
                            <div class="form-group text-left">
                                <label class="control-label">Location</label>
                                <SingleSelectDropdown
                                    placeholder="Select Location"
                                    options={this.state.Locations}
                                    name="Location"
                                    value={this.state.searchObject.Location}
                                    onChange={this.handleSearchObject}
                                />

                            </div>

                        </div>
                    </div>
                    : null}

            </div>
        )
    }
}

export default GetJobs
