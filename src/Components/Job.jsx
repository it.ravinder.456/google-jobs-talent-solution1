import React from 'react'
import { Segment } from 'semantic-ui-react'

const Job = (props) => (
    <Segment.Group >
        <Segment><u><a className="link"><h3>{props.item.JobTitle}</h3></a></u> </Segment>
        <Segment.Group horizontal>
            <Segment><strong> Company : </strong> {props.item.CompanyName}</Segment>
            <Segment><strong>Experience : </strong>{props.item.Experience} </Segment>
            <Segment><strong>Location : </strong>{props.item.Location}</Segment>
        </Segment.Group>
        <Segment><strong> Description : </strong>{props.item.Description} </Segment>
    </Segment.Group>
)

export default Job