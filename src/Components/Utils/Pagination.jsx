import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { globalHardCodedConstants } from "./SemanticUIComponents/Helpers";
// require("bootstrap/less/bootstrap.less");

 
class Paginations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      error:null,
      errorInfo:false
    };
  }
 
  handlePageChange=(pageNumber)=> {
      this.setState({activePage: pageNumber});
      this.props.onPageChange(pageNumber);
  }
 
  render() {
      return (
        <div className="text-center">
          <Pagination
            prevPageText="Prev"
            nextPageText="Next"
            firstPageText="First"
            lastPageText="Last"
            activePage={this.props.currentPage}
            itemsCountPerPage={globalHardCodedConstants("PAGINATION_ITEMS_LENGTH")}
            totalItemsCount={this.props.totalItemsCount}
            pageRangeDisplayed={this.props.pageRange ? this.props.pageRange : 10}
            onChange={this.handlePageChange}
            hideFirstLastPages={this.props.itemsCount < 200 ? true : false}
          />
        </div>
      );
  
  }
}
export default Paginations;