import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react';

export class SingleSelectDropdown extends Component {
    render() {
        return (
            <React.Fragment>
                <Dropdown
                    onClick={this.props.onClick}
                    defaultValue={this.props.defaultValue}
                    name={this.props.name}
                    id={this.props.id ? this.props.id : ""}
                    clearable
                    fluid
                    search
                    value={this.props.value}
                    selection
                    options={this.props.options}
                    onChange={this.props.onChange}
                    placeholder={this.props.placeholder}
                    disabled={this.props.disabled}
                    error={this.props.error}
                    onSearchChange={this.props.onSearchChange}
                />
            </React.Fragment>
        )
    }
}

export default SingleSelectDropdown;