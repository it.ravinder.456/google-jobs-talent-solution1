import _ from "lodash"
/**
 * To get distinct values from array and convert into option Values
 * array = collection of objects
 * distinctValue = Specific KEY in the Array
 */
export function getDistinctValues(array, distinctValue) {
    var optionValues = [];
    const distinctValues = [
      ...new Set(array.map(object => object[distinctValue]))
    ];
    distinctValues.map((value, index) => {
      optionValues[index] = {};
      optionValues[index]["value"] = value;
      optionValues[index]["text"] = value;
    });
    return optionValues;
  }

  /**
   * 
   * @param {items:the total array of objects} items 
   * @param {pageNumber:The current active page number} pageNumber 
   * @param {*} pageSize 
   */
  export function paginate(items, pageNumber, pageSize = 10) {
    pageSize = globalHardCodedConstants("PAGINATION_ITEMS_LENGTH");
    const startIndex = (pageNumber - 1) * pageSize;
    return _(items)
      .slice(startIndex)
      .take(pageSize)
      .value();
  }

  /**
 * To set Pagination_items
 */
export function globalHardCodedConstants(key) {
  const globalConstants = {
    PAGINATION_ITEMS_LENGTH: 5
  };
  return globalConstants[key];
}

/**
 *
 * @param {list of objects & current page} objects
 */
export function getPagedData(objects, currentPage = 1) {
  let filteredObjects = paginate(objects, currentPage);
  return filteredObjects;
}
  