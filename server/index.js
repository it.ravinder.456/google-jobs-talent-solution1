const http = require('http');
const cors = require('cors');
const express = require('express');
const bodyParser=require('body-parser')
const router = require('./router');
const app = express();
const PORT = 5000;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);
// app.listen(process.env.PORT || PORT, () => console.log(`Server started on port ${PORT}`))
app.listen(process.env.PORT)
