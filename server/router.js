const express = require("express");
const router = express.Router();
var jobsMaster = require("./jobs.json").jobs;
const _ = require("lodash");

router.post("/getJobList", (req, res) => {
  var filters = _.pickBy(req.body);
  filters = removeEmpty(filters);
  var jobs = [];
  jobs = _.filter(jobsMaster, filters)
  res.send({ data: jobs }).status(200);
});

router.get("/GetAttributes", (req, res) => {
  res.send({ data: jobsMaster }).status(200);
});
router.get("/", (req, res) => {
  res.send("Server is ip and running").status(200);
});

const removeEmpty = (obj) => {
  Object.keys(obj).forEach((k) => !obj[k] && delete obj[k]);
  return obj;
};

module.exports = router;